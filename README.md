# Reading Early Medicine (REM) project

This is the project for the database and website about English vernacular medical books by the research group of Elaine Leong (2012-2019).

## Project database

The project database is the FileMaker database "db-vernacular-medical-books.fp12" on the MPIWG FileMaker server.

## Project website

The project website at https://reademed.mpiwg-berlin.mpg.de/ uses a [MPIWG microsite](https://gitlab.gwdg.de/MPIWG/central-services/drupal/mpiwg-microsite) with the profile "vernacular".

Data is imported from the FileMaker database into Drupal using a custom migration.

## Data export script

The Python script `rem-fm-exporter.py` can be used to read XML export files from FileMaker and export CSV files. The script needs the following files in the runtime directory (exported from FileMaker using the "biblio_xml" etc. layouts):
- `biblio.xml`
- `persons.xml`
- `places.xml`
- `streets.xml`

and these additional information files:
- `places-extrainfo.csv`
- `streets-extrainfo.csv`
- `genres-extrainfo.csv`
- `topics-extrainfo.csv`

and it produces the following output files:
- `biblio.csv`
- `persons.csv`
- `places.csv`
- `streets.csv`
- `topics.csv`
- `genres.csv`
- `author_occupations.csv`
