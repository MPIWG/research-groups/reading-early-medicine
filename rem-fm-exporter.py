#!/usr/bin/env python3

import xml.etree.ElementTree as ET
import csv
import logging

__version__ = '2.2'

# FileMaker XML namespace
FMPXMLNS = {'': 'http://www.filemaker.com/fmpxmlresult'}

# convert FM repeat fields into extra columns
repeat_extra_cols = False
# separator for FM repeat fields when not extra columns
repeat_separator = ';'

# help debugging rowid-label replacement
debug_id_and_label = False

# ids that need to be mapped to labels
field_id_type = {
    'AuthorId': 'persons',
    'AuthorPlace': 'places',
    'BooksellerId': 'persons',
    'BooksellerPlace': 'places',
    'PrinterId': 'persons',
    'PrinterPlace': 'places',
    'PublisherId': 'persons',
    'PublisherPlace': 'places',
    'PlaceId': 'places',
    'StreetId': 'streets'
}

def load_xml_file(filename):
    """
    Load and parse filename. 
    Returns etree root Element.
    """
    tree = ET.parse(filename)
    return tree.getroot()

def read_metadata(doc):
    """
    Read FileMaker METADATA element.
    Returns list of field definition dicts.
    """
    meta = doc.find('METADATA', FMPXMLNS)
    if not meta:
        raise RuntimeError('Unable to find METADATA element!')
    
    fields = []
    for f in meta.findall('FIELD', FMPXMLNS):
        fields.append({'name': f.get('NAME'), 'type': f.get('TYPE')})

    return fields

def read_resultset(doc, fields):
    """
    Read FileMaker RESULTSET element according to fields list.
    Returns list of column dicts with field data.
    """
    res = doc.find('RESULTSET', FMPXMLNS)
    if not res:
        raise RuntimeError('Unable to find RESULTSET element!')
    
    rowcnt = int(res.get('FOUND'))
    rows = []
    for row in res.findall('ROW', FMPXMLNS):
        recid = row.get('RECORDID')
        cols = {}
        fidx = 0
        for col in row.findall('COL', FMPXMLNS):
            datas = []
            for data in col.findall('DATA', FMPXMLNS):
                text = data.text
                if isinstance(text, str):
                    text = text.strip()
                    
                datas.append(text)
                
            name = fields[fidx]['name']
            if name in cols:
                logging.warning(f"Duplicate column name {name}!")
                
            if datas:
                cols[name] = datas
                   
            fidx += 1

        if cols:
            cols['_recid'] = recid
            rows.append(cols)
        else:
            logging.debug(f"empty row (rowid={recid})")

    if len(rows) != rowcnt:
        logging.warning(f"Number of rows {len(rows)} does not match FOUND {rowcnt}!")
                
    return rows

def check_data(dataset, unique_label=None):
    """
    Check consistency of dataset.
    Returns dict with dataset by rowid.
    """
    idset = {}
    label_set = set()
    rowcnt = 0
    for row in dataset:
        rowcnt += 1
        recid = row['_recid']
        if not 'ID' in row:
            logging.warning(f"Missing ID in row {rowcnt}! row={row}")
            continue
            
        idcol = row['ID']
        
        if len(idcol) == 0:
            logging.warning(f"Empty rowid in row {rowcnt} ({recid=})!")
            continue
        elif len(idcol) > 1:
            logging.warning(f"Multiple ids {idcol} in row {rowcnt} ({recid=})!")

        rowid = idcol[0]
        if rowid in idset:
            logging.warning(f"Duplicate ID {rowid} ({recid=})!")
            
        if unique_label is not None:
            # create unique label by combining columns from unique_label
            label = ', '.join([row[f][0] for f in unique_label if (f in row and row[f][0])])
            if debug_id_and_label:
                label += f" [{rowid}]"

            if label.lower() in label_set:
                logging.warning(f"Unique label '{label}' is not unique!")
                label += f" [{rowid}]"
            else:
                label_set.add(label.lower())
                
            row['label'] = label
        
        idset[rowid] = row

    return idset

def load_data(filename):
    """
    Load and parse XML file at filename.
    Returns dataset (list of row dicts), fields (list of field dicts).
    """
    doc = load_xml_file(filename)
    fields = read_metadata(doc)
    data = read_resultset(doc, fields)
    return data, fields

def _expand_colnames(colnames):
    """
    expand list of column names for multi-value columns.
    returns list of column names.
    """
    newcolnames = []
    for name in colnames:
        if name in {'PlaceId', 'PlaceOf'}:
            # replaced with role-place columns below
            continue
        
        elif repeat_extra_cols and name in {'BooksellerId', 'PrinterId', 'PublisherId'}:
            # add muliple columns for multi-value fields
            for i in range(1, 6):
                newcolnames.append(f"{name}:{i}")
                if debug_id_and_label:
                    newcolnames.append(f"{name}_id:{i}")
                
        else:
            newcolnames.append(name)
            
    if 'AuthorId' in colnames:
        # add role-place columns for biblio (generated by _process_row)
        if repeat_extra_cols:
            # create extra columns for every possible id
            newcolnames.append(f"AuthorPlace:1")
            if debug_id_and_label:
                newcolnames.append(f"AuthorId_id:1")
                newcolnames.append(f"AuthorPlace_id:1")
            
            for i in range(1, 6):
                newcolnames.append(f"BooksellerPlace:{i}")
                if debug_id_and_label:
                    newcolnames.append(f"BooksellerPlace_id:{i}")
            for i in range(1, 6):
                newcolnames.append(f"PrinterPlace:{i}")
                if debug_id_and_label:
                    newcolnames.append(f"PrinterPlace_id:{i}")
            for i in range(1, 6):
                newcolnames.append(f"PublisherPlace:{i}")
                if debug_id_and_label:
                    newcolnames.append(f"PublisherPlace_id:{i}")
        else:
            # keep single column, values will have separator character
            newcolnames.append(f"AuthorPlace")
            if debug_id_and_label:
                newcolnames.append(f"AuthorId_id")
                newcolnames.append(f"AuthorPlace_id")            
            newcolnames.append(f"BooksellerPlace")
            if debug_id_and_label:
                    newcolnames.append(f"BooksellerPlace_id")
            newcolnames.append(f"PrinterPlace")
            if debug_id_and_label:
                newcolnames.append(f"PrinterPlace_id")
            newcolnames.append(f"PublisherPlace")
            if debug_id_and_label:
                newcolnames.append(f"PublisherPlace_id")
   
    if 'StreetId' in colnames and debug_id_and_label:
        newcolnames.append('StreetId_id')
        
    return newcolnames

def _nextidx(row, key):
    """find the next free index for key+index in dict row.""" 
    for i in range(1,6):
        if not key + str(i) in row:
            return i
        
    raise KeyError(f"Unable to find free index for key={key}")

def _get_id_label(key, val, idsets):
    """returns the label for the given rowid field"""
    id_type = field_id_type[key]
    ids = idsets[id_type]
    if val not in ids:
        raise ValueError(f"{key}='{val}' not found in idset {id_type} when looking for label!")
    
    label = ids[val]['label']
    return label

def _process_row(row, idsets):
    """
    Process row with fields from XML into format for CSV.
    Resolves multi-select and multi-value fields.
    Replaces numerical IDs with labels.
    
    returns dict row.
    """
    newrow = {}
    for key, val in row.items():
        # ignore _rowid
        if key in {'_recid', 'PlaceOf'}:
            continue

        if isinstance(val, list):
            if len(val) == 1:
                # single element list
                val = val[0]
                if not val:
                    continue
                
                if key in {'AuthorOccupation', 'Genre', 'Topic'}:
                    # replace multi-select separator
                    val = val.replace('\n', ';')
                    # save unique values in idset
                    setkey = key.lower()
                    if not setkey in idsets:
                        idsets[setkey] = set()
                        
                    items = [v.strip().lower() for v in val.split(';')]
                    for item in items:
                        idsets[setkey].add(item)
                        
                    val = ';'.join(items)
                    
                elif key in field_id_type:
                    # look up and exchange id with unique label
                    l = _get_id_label(key, val, idsets)
                    if debug_id_and_label:
                        newrow[f"{key}_id"] = val
                    val = l
                    
                newrow[key] = val
            
            else:
                # val contains multiple elements
                if key == 'PlaceId':
                    # process PlaceId
                    idx = 0
                    for v in val:
                        idx += 1
                        if not v:
                            continue

                        if not v in idsets['places']:
                            logging.warning(f"PlaceId {v} does not match existing place!")

                        # check matching PlaceOf                        
                        placeof = row['PlaceOf'][idx - 1]
                        if placeof:
                            # look up and exchange id with unique label
                            l = _get_id_label(key, v, idsets)
                            if repeat_extra_cols:
                                pidx = _nextidx(row, f"{placeof}Place:")
                                newrow[f"{placeof}Place:{pidx}"] = l
                                if debug_id_and_label:
                                    newrow[f"{placeof}Place_id:{pidx}"] = v

                            else:
                                if f"{placeof}Place" in newrow:
                                    newrow[f"{placeof}Place"] += repeat_separator + l
                                    if debug_id_and_label:
                                        newrow[f"{placeof}Place_id"] += repeat_separator + v
                                else:
                                    newrow[f"{placeof}Place"] = l
                                    if debug_id_and_label:
                                        newrow[f"{placeof}Place_id"] = v
                            
                        else:
                            # no PlaceOf: try matching ids
                            if row['BooksellerId'][idx - 1]:
                                placeof = 'Bookseller'
                                if not row['BooksellerId'][idx - 1] in idsets['persons']:
                                    logging.warning(f"BooksellerId {row['BooksellerId'][idx - 1]} does not match existing person!")
                            elif row['PrinterId'][idx - 1]:
                                placeof = 'Printer'
                                if not row['PrinterId'][idx - 1] in idsets['persons']:
                                    logging.warning(f"PrinterId {row['PrinterId'][idx - 1]} does not match existing person!")
                            elif row['PublisherId'][idx - 1]:
                                placeof = 'Publisher'
                                if not row['PublisherId'][idx - 1] in idsets['persons']:
                                    logging.warning(f"PublisherId {row['PublisherId'][idx - 1]} does not match existing person!")
                            elif row['AuthorId'][0]:
                                placeof = 'Author'
                                if not row['AuthorId'][0] in idsets['persons']:
                                    logging.warning(f"AuthorId {row['AuthorId'][0]} does not match existing person!")
                            else:
                                logging.warning(f"Missing PlaceOf for PlaceId:{idx}={v} in row {row['ID'][0]} recid={row['_recid']}")
                                
                            if placeof:
                                # look up and exchange id with unique label
                                l = _get_id_label(key, v, idsets)
                                if repeat_extra_cols:
                                    pidx = _nextidx(row, f"{placeof}Place:")
                                    logging.warning(f"Replaced missing PlaceOf with {placeof} for PlaceId:{idx}={v} in row {row['ID'][0]} recid={row['_recid']}")
                                    newrow[f"{placeof}Place:{pidx}"] = l
                                    if debug_id_and_label:
                                        newrow[f"{placeof}Place_id:{pidx}"] = v
                                else:
                                    logging.warning(f"Replaced missing PlaceOf with {placeof} for PlaceId:{idx}={v} in row {row['ID'][0]} recid={row['_recid']}")
                                    if f"{placeof}Place" in newrow:
                                        newrow[f"{placeof}Place"] += repeat_separator + l
                                        if debug_id_and_label:
                                            newrow[f"{placeof}Place_id"] += repeat_separator + v
                                    else:
                                        newrow[f"{placeof}Place"] = l
                                        if debug_id_and_label:
                                            newrow[f"{placeof}Place_id"] = v

                else:
                    # copy list into fields
                    idx = 0
                    for v in val:
                        idx += 1
                        if not v:
                            continue
                        
                        if key in field_id_type:
                            # look up and exchange id with unique label
                            l = _get_id_label(key, v, idsets)
                            if repeat_extra_cols:
                                newkey = f"{key}:{idx}"
                                newrow[newkey] = l
                                if debug_id_and_label:
                                    newrow[f"{key}_id:{idx}"] = v
                                    
                            else:
                                if key in newrow:
                                    newrow[key] += repeat_separator + l
                                    if debug_id_and_label:
                                        newrow[f"{key}_id"] = repeat_separator + v
                                else:
                                    newrow[key] = l
                                    if debug_id_and_label:
                                        newrow[f"{key}_id"] = v
                                    
                        else:
                            if repeat_extra_cols:
                                newkey = f"{key}:{idx}"
                                newrow[newkey] = v
                            else:
                                if key in newrow:
                                    newrow[key] += repeat_separator + l
                                else:
                                    newrow[key] = l
                    
        else:
            # non-list value
            if key in field_id_type:
                l = _get_id_label(key, val, idsets)
                newrow[key] = l
                if debug_id_and_label:
                    newrow[f"{key}_id"] = v
            else:
                newrow[key] = val
            
    return newrow

def save_dataset_csv(dataset, fields, idsets, filename):
    """
    process and save dataset using field list fields to CSV file filename.
    adds information to idsets while processing.
    """
    fieldnames = [f['name'] for f in fields]
    colnames = _expand_colnames(fieldnames)
    if 'label' in dataset[0]:
        colnames.append('label')

    with open(filename, 'w') as f:
        rowcnt = 0
        writer = csv.DictWriter(f, fieldnames=colnames)
        writer.writeheader()
        for row in dataset:
            rowcnt += 1
            try:
                writer.writerow(_process_row(row, idsets))
            except Exception as e:
                logging.error(f"Error writing row {rowcnt}: {e}")

def save_list_csv(items, fieldname, filename):
    """
    save list items to CSV file filename.
    """
    with open(filename, 'w') as f:
        writer = csv.writer(f)
        # write header
        writer.writerow([fieldname])
        # write list
        for item in items:
            writer.writerow([item])

def load_extra_csv(filename):
    data = []
    with open(filename, 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            data.append(row)
    
    return data

def map_extra_data(dataset, fields, extradata, fieldmap, match_on):
    """
    add fields from extradata to dataset where fields in match_on are matching.
    match_on is a list of field names in extradata.
    
    returns dataset, fields
    """
    # only add extra fields that are not matched
    extra_fields = [f for f in fieldmap if not f in match_on]
    # index extra data by matched fields
    extra_idset = {}
    for row in extradata:
        # construct key from values in match_on fields
        rowid = tuple([row[m] for m in match_on])
        extra_idset[rowid] = row
        
    unused_ids = set(extra_idset.keys())
    # match extra data to dataset
    new_dataset = []
    for row in dataset:
        # construct key from values in mapped match_on fields
        rowid = tuple([row[fieldmap[m]][0] for m in match_on])
        if rowid in extra_idset:
            unused_ids.discard(rowid)
            extra_row = extra_idset[rowid]
            # append row and extra fields
            new_dataset.append(row | {fieldmap[k]: [extra_row[k]] for k in extra_fields})
        else:
            # append existing row
            new_dataset.append(row)

    if unused_ids: 
        logging.warning(f"unmatched extra data for rowid: {unused_ids}")
        
    return new_dataset, fields + [{'name': fieldmap[f]} for f in extra_fields]

##
## main
##
logging.basicConfig(level='DEBUG')

# load FileMaker tables from XML
logging.info(f"Loading XML file biblio.xml")
biblio_data, biblio_fields = load_data('biblio.xml')
logging.info(f"  read {len(biblio_data)} rows.")

logging.info(f"Loading XML file persons.xml")
person_data, person_fields = load_data('persons.xml')
logging.info(f"  read {len(person_data)} rows.")

logging.info(f"Loading XML file places.xml")
place_data, place_fields = load_data('places.xml')
logging.info(f"  read {len(place_data)} rows.")

logging.info(f"Loading XML file streets.xml")
street_data, street_fields = load_data('streets.xml')
logging.info(f"  read {len(street_data)} rows.")

# add place extra info from CSV
logging.info(f"Adding extra info for places...")
#extra_place_data = load_extra_csv('places-extrainfo.csv')
# just add emtpy extra fields to csv
extra_place_data = [{'Term Name': '[unspecified]', 'City':'Norwich', 'Latitude':'', 'Longitude':''}]
extra_place_field_map = {
    'Term Name': 'Place',
    'City': 'City',
    'Latitude': 'Latitude', 
    'Longitude': 'Longitude'}
place_data, place_fields = map_extra_data(place_data, place_fields, 
                                            extra_place_data, extra_place_field_map, 
                                            ['Term Name', 'City'])

# add street extra info from CSV
logging.info(f"Adding extra info for streets from streets-extrainfo.csv...")
extra_street_data = load_extra_csv('streets-extrainfo.csv')
extra_street_field_map = {
    'Street': 'Street',
    'City': 'City',
    'Latitude': 'Latitude', 
    'Longitude': 'Longitude', 
    'Maps of Early Modern England': 'MOEML_URL', 
    'Old Maps Online': 'OldMaps_URL'}
street_data, street_fields = map_extra_data(street_data, street_fields, 
                                            extra_street_data, extra_street_field_map, 
                                            ['Street', 'City'])

# check data and create labels
logging.info("Checking missing or duplicate ids for biblio...")
biblio_ids = check_data(biblio_data)
logging.info("Checking missing or duplicate ids for persons...")
person_ids = check_data(person_data, unique_label=['LastName', 'FirstName'])
logging.info("Checking missing or duplicate ids for places...")
place_ids = check_data(place_data, unique_label=['Place', 'City'])
logging.info("Checking missing or duplicate ids for streets...")
street_ids = check_data(street_data, unique_label=['Street', 'City'])

# process and save data as CSV
idsets = {
    'biblio': biblio_ids, 
    'persons': person_ids, 
    'places': place_ids, 
    'streets': street_ids}
logging.info(f"Processing and saving biblio dataset to CSV file biblio.csv")
save_dataset_csv(biblio_data, biblio_fields, idsets, 'biblio.csv')
logging.info(f"Processing and saving persons dataset to CSV file persons.csv")
save_dataset_csv(person_data, person_fields, idsets, 'persons.csv')
logging.info(f"Processing and saving places dataset to CSV file places.csv")
save_dataset_csv(place_data, place_fields, idsets, 'places.csv')
logging.info(f"Processing and saving streets dataset to CSV file streets.csv")
save_dataset_csv(street_data, street_fields, idsets, 'streets.csv')

# create genres dataset
genre_data = [{'Genre': [g]} for g in idsets['genre']]
genre_fields = [{'name': 'Genre'}]
# add genres extra info from CSV
logging.info(f"Adding extra info for genres from genres-extrainfo.csv...")
extra_genre_data = load_extra_csv('genres-extrainfo.csv')
extra_genre_field_map = {
    'Term Name': 'Genre',
    'Description': 'Description'}
genre_data, genre_fields = map_extra_data(genre_data, genre_fields, 
                                            extra_genre_data, extra_genre_field_map, 
                                            ['Term Name'])
logging.info(f"Processing and saving genres dataset to CSV file genres.csv")
save_dataset_csv(genre_data, genre_fields, idsets, 'genres.csv')

# create topics dataset
topic_data = [{'Topic': [g]} for g in idsets['topic']]
topic_fields = [{'name': 'Topic'}]
# add topics extra info from CSV
logging.info(f"Adding extra info for topics from topics-extrainfo.csv...")
extra_topic_data = load_extra_csv('topics-extrainfo.csv')
extra_topic_field_map = {
    'Term Name': 'Topic',
    'Description': 'Description'}
topic_data, topic_fields = map_extra_data(topic_data, topic_fields, 
                                            extra_topic_data, extra_topic_field_map, 
                                            ['Term Name'])
logging.info(f"Processing and saving topics dataset to CSV file topics.csv")
save_dataset_csv(topic_data, topic_fields, idsets, 'topics.csv')

logging.info(f"Saving author_occupation list to CSV file author_occupations.csv")
save_list_csv(list(idsets['authoroccupation']), 'author_occupation', 'author_occupations.csv')

logging.info('Done.')