#!/usr/bin/env python3

import csv
import logging

__version__ = '0.1'

def load_csv(filename):
    data = []
    header = None
    # process BOM with utf-8-sig
    with open(filename, 'r', encoding='utf-8-sig') as f:
        reader = csv.reader(f)
        for row in reader:
            if header is None:
                header = row
            else:
                data.append(row)
    
    return header, data


##
## main
##
logging.basicConfig(level='DEBUG')

wp_biblio = load_csv('jhu-data/biblio.csv')
cols, rows = wp_biblio
logging.debug(f"wp_biblio: {len(rows)} rows, {len(cols)} columns={list(enumerate(cols))}")
bib_id = 121
bib_title = 1
biblio = [{'id': row[bib_id], 'title': row[bib_title]} for row in rows]
logging.debug(f"{biblio=}")